import numpy as np
from scipy import sparse
from scipy.sparse import linalg as slinalg
from sklearn.linear_model import Ridge
from sklearn.model_selection import train_test_split

import gc
import logging
import time

from utils import calculate_metrics

class ESN:

	def __init__(self, N_x, connect, scaleW_in, lamda, verbose, rho, alpha):
		"""Инициализация гиперпараметров и весов"""

		self.verbose = verbose
		logging.info("Инициализация гиперпараметров.")
		self.N_x = N_x
		self.connect = connect
		self.scaleW_in = scaleW_in
		self.lamda = lamda
		self.rho= rho
		self.alpha = alpha
		logging.info(self.__dict__)

	def __init_matrices(self):
		""""Инициализация матрицы весов для входного вектора и резервуара"""

		self.W_in = (2.0*np.random.rand(self.N_x, self.num_dim)-1.0) / \
					(2.0*self.scaleW_in)

		converged = False
		i = 0
		# repeat because could not converge to find eigenvalues
		while(not converged):
			i += 1

			# generate sparse, uniformly distributed weights
			self.W = sparse.rand(self.N_x, self.N_x,
								density=self.connect).todense()

			# ensure that the non-zero values are uniformly distributed
			self.W[np.where(self.W > 0)] -= 0.5

			try:
				# get the largest eigenvalue
				eig, _ = slinalg.eigs(self.W, k=1, which='LM')
				converged = True
			except:
				print('not converged ', i)
				continue

		# adjust the spectral radius
		self.W /= np.abs(eig)/self.rho

	def compute_state_matrix(self, x_in):
		# number of instances
		n = x_in.shape[0]
		# the state matrix to be computed
		logging.info("Размерность резервуара: {0}".format((n, self.T, self.N_x)))
		X_t = np.zeros((n, self.T, self.N_x), dtype=np.float64)
		# previous state matrix
		X_t_1 = np.zeros((n, self.N_x), dtype=np.float64)
		# loop through each time step
		for t in range(self.T):
			logging.debug("Состояние резервуара t = {0} / T = {1}".format(t, self.T))
			# get all the time series data points for the time step t
			curr_in = x_in[:, t, :]
			# calculate the linear activation
			curr_state = np.tanh(self.W_in.dot(curr_in.T)+self.W.dot(X_t_1.T)).T
			# apply leakage
			curr_state = (1-self.alpha)*X_t_1 + self.alpha*curr_state
			# save in previous state
			X_t_1 = curr_state
			# save in state matrix
			X_t[:, t, :] = curr_state

		return X_t

	def reshape_prediction(self, y_pred, num_instances, length_series):
		# reshape so the first axis has the number of instances
		new_y_pred = y_pred.reshape(
			num_instances, length_series, y_pred.shape[-1])
		# average the predictions of instances
		new_y_pred = np.average(new_y_pred, axis=1)
		return new_y_pred

	def fit(self, x_train, y_train, x_test, y_test):
		self.num_dim = x_train.shape[2]  # Размерность временного ряда
		self.T = x_train.shape[1]  # Длительность временного ряда
		self.N = x_train.shape[0]

		start_time = time.time()
		logging.info("Начало обучения")
		logging.info("Инициализация матриц весов W и W_in")
		# init the matrices
		self.__init_matrices()
		logging.info("Вычисление состояний резервуара X")
		# compute the state matrices which is the new feature space
		state_matrix = self.compute_state_matrix(x_train)
		# add the input to form the new feature space and transform to
		# the new feature space to be feeded to the classifier
		logging.info("Конкатенация U и X")
		new_x_train = np.concatenate((x_train, state_matrix), axis=2).reshape(
			self.N * self.T, self.num_dim+self.N_x)
		# memory free
		state_matrix = None
		gc.collect()
		# transform the corresponding labels
		new_labels = np.repeat(y_train, self.T, axis=0)
		# new model
		ridge_classifier = Ridge(alpha=self.lamda)
		logging.info("Ridge fitting...")
		# fit the new feature space
		ridge_classifier.fit(new_x_train, new_labels)
		duration = time.time() - start_time
		logging.info("Обучение завершено: {0}".format(duration))

		self.ridge_classifier = ridge_classifier
		# self.predict(x_test, y_test)


	def predict(self, x_test ):
		logging.info("Начало проверки")
		# compute state matrix for validation set
		state_matrix = self.compute_state_matrix(x_test)
		# add the input to form the new feature space and transform to
		# the new feature space to be feeded to the classifier
		new_x_test = np.concatenate((x_test, state_matrix), axis=2).reshape(
			x_test.shape[0] * self.T, self.num_dim+self.N_x)
		logging.info("Прогнозирование...")
		y_pred_test = self.ridge_classifier.predict(new_x_test)
		y_pred_test = self.reshape_prediction(
			y_pred_test, x_test.shape[0], self.T)
		logging.info("Прогнозирование завершено")
		return y_pred_test