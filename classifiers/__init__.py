from .mlp import MLP
from .cnn import CNN
from .esn import ESN
from .resnet import RESNET
