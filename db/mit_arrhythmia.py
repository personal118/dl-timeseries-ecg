import numpy as np
import wfdb
import random
import pandas as pd
import logging

logger = logging.getLogger()

#Путь до выборки
data_path = './db/mit-bih-arrhythmia-database-1.0.0/'

#Список пациентов
pts = ['100','101','102','103','104','105','106','107',
       '108','109','111','112','113','114','115','116',
       '117','118','119','121','122','123','124','200',
       '201','202','203','205','207','208','209','210',
       '212','213','214','215','217','219','220','221',
       '222','223','228','230','231','232','233','234']

# Список некорректных аннотаций и аннотаций символизирующие аномальные сердечные ритмы 
nonbeat = ['[','!',']','x','(',')','p','t','u','`',
           '\'','^','|','~','+','s','T','*','D','=','"','@','Q','?']
abnormal = ['L','R','V','/','A','f','F','j','a','E','J','e','S']

num_sec = 1  
fs = 360

def load_ecg(file):
    # load the ecg
    # example file: 'mit-bih-arrhythmia-database-1.0.0/101'
    
    # load the ecg
    record = wfdb.rdrecord(file)
    # load the annotation
    annotation = wfdb.rdann(file, 'atr')
    
    # extract the signal
    p_signal = record.p_signal
    
    # verify frequency is 360
    assert record.fs == 360, 'sample freq is not 360'
    
    # extract symbols and annotation index
    atr_sym = annotation.symbol
    atr_sample = annotation.sample
    
    return p_signal, atr_sym, atr_sample  

def make_dataset(pts, num_sec, fs, abnormal):
    # function for making dataset ignoring non-beats
    # input:
    # pts - list of patients
    # num_sec = number of seconds to include before and after the beat
    # fs = frequency
    # output: 
    #   X_all = signal (nbeats , num_sec * fs columns)
    #   Y_all = binary is abnormal (nbeats, 1)
    #   sym_all = beat annotation symbol (nbeats,1)
    
    # initialize numpy arrays
    num_cols = 2*num_sec * fs
    X_all = np.zeros((1,num_cols, 1))
    Y_all = np.zeros((1,2))
    sym_all = []
    
    # list to keep track of number of beats across patients
    max_rows = []

    logger.info("Начинаем сбор данных, два класса: Нормальный ритм и Ненормальный ритм сердцебиения")
    
    for index, pt in enumerate(pts):
        logger.debug("Собираем данные по пациенту: {0}. {1} / {2}".format(pt, index+1, len(pts)))
        file = data_path + pt
        
        p_signal, atr_sym, atr_sample = load_ecg(file)
        
        # grab the first signal
        p_signal = p_signal[:,0]
        
        # make df to exclude the nonbeats
        df_ann = pd.DataFrame({'atr_sym':atr_sym,
                              'atr_sample':atr_sample})
        df_ann = df_ann.loc[df_ann.atr_sym.isin(abnormal + ['N'])]
        
        X,Y,sym = build_XY(p_signal,df_ann, num_cols, abnormal)
        sym_all = sym_all+sym
        max_rows.append(X.shape[0])
        X_all = np.append(X_all,X,axis = 0)
        Y_all = np.append(Y_all,Y,axis = 0)
    # drop the first zero row
    X_all = X_all[1:,:]
    Y_all = Y_all[1:,:]

    logger.info("Сбор даннных завершен")
    
    # check sizes make sense
    assert np.sum(max_rows) == X_all.shape[0], 'number of X, max_rows rows messed up'
    assert Y_all.shape[0] == X_all.shape[0], 'number of X, Y rows messed up'
    assert Y_all.shape[0] == len(sym_all), 'number of Y, sym rows messed up'

    return X_all, Y_all, sym_all

def build_XY(p_signal, df_ann, num_cols, abnormal):
    # this function builds the X,Y matrices for each beat
    # it also returns the original symbols for Y
    
    num_rows = len(df_ann)

    X = np.zeros((num_rows, num_cols, 1))
    Y = np.zeros((num_rows, 2))
    sym = []
    
    # keep track of rows
    max_row = 0

    for atr_sample, atr_sym in zip(df_ann.atr_sample.values,df_ann.atr_sym.values):

        left = max([0,(atr_sample - num_sec*fs) ])
        right = min([len(p_signal),(atr_sample + num_sec*fs) ])
        x = np.array(p_signal[left: right])
        if len(x) == num_cols:
            X[max_row,:] = x.reshape((len(x), 1))
            Y[max_row,int(atr_sym in abnormal)] = 1
            sym.append(atr_sym)
            max_row += 1
    X = X[:max_row,:]
    Y = Y[:max_row,:]
    return X,Y,sym

def get_dataset():
    logging.info(num_sec)
    pts_train = random.sample(pts, 36)
    pts_valid = [pt for pt in pts if pt not in pts_train]
    logger.info("Сборка выборки для обучения")
    X_train, y_train, sym_train = make_dataset(pts_train, num_sec, fs, abnormal)
    logger.info("Сборка выборки для валидации")
    X_valid, y_valid, sym_valid = make_dataset(pts_valid, num_sec, fs, abnormal)

    return X_train, y_train, X_valid, y_valid